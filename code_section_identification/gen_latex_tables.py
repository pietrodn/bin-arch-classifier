import argparse
from pathlib import Path
import json
import numpy as np

metric_names = {
    'accuracy': 'Accuracy',
    'precision': 'Precision',
    'recall': 'Recall',
    'f1': 'F1',
}

def main():
    parser = argparse.ArgumentParser(
        description='Generate LaTeX tables from the results of the evaluation phase.')
    parser.add_argument('evaluation_folder')
    args = parser.parse_args()

    json_data = {}
    for json_file in Path(args.evaluation_folder).glob('*.json'):

        json_res = json.loads(
            json_file.read_text(),
            parse_float=lambda x: round(float(x), 4) # precision of floats
        )

        json_data[json_file.stem] = json_res


    tex_source = parse_results(json_data)
    outfile = Path(args.evaluation_folder) / 'results.tex'
    outfile.write_text(tex_source)

    print("Wrote {}".format(outfile))


def parse_results(json_data):
    headers = ('Arch', 'Support', 'Code \\%') + tuple(metric_names.values()) + ('Time (s)',)
    headers = tuple('\\textbf{' + h + '}' for h in headers)

    table = []
    for (arch, data) in json_data.items():
        metric_key = 'cv_metrics' if 'cv_metrics' in data else 'holdout_metrics'
        row = (
            '\\texttt{' + arch + '}',
            data['num_binaries'],
            round(data['avg_code_fraction'] * 100, 2),
        ) + tuple(data[metric_key][k][0] for k in metric_names.keys()) + (round(data['cv_time'], 1),)
        table.append(row)

    return gen_latex_table(table, headers)


def gen_latex_table(values, headers, format_string=None, footer=None):

    def format_row(t):
        vals = " & ".join((str(x) for x in t))
        return vals + " \\\\"

    if format_string is None:
        format_string = ''.join('l' * len(headers))

    out = "\\begin{tabular}{" + format_string + "}"
    out += "\n" + format_row(headers) + "\n\hline\n"
    out += "\n".join([format_row(row) for row in values])
    if footer:
        out += "\n\hline\n" + format_row(footer)
    out += "\n\\hline\n\\end{tabular}"
    return out

if __name__ == '__main__':
    main()
