from scipy import sparse
import filebytes.elf
import filebytes.pe
import filebytes.mach_o
import magic
import numpy as np
import os
from sklearn.model_selection import train_test_split


class SectionPreprocess:

    def __init__(self, ground_truth=True):
        self.extract_ground_truth = ground_truth

    def _binary_to_matrix(self, path):
        """Convert the given object file to a matrix (num_bytes, 256) where each row is a one-hot array for each byte.

        This is the format required by pystruct."""

        with open(path, 'rb') as f:
            blob = f.read()
        byte_arr = np.frombuffer(blob, dtype='uint8') # array of integers [0, 255]

        # picks the correct rows from the identity matrix
        feat_byte_0 = sparse.csr_matrix((np.ones(len(byte_arr)), (np.arange(byte_arr.shape[0]), byte_arr)))

        return feat_byte_0

    def _code_mask(self, path):
        """Returns a mask ranging over the bytes in the binary:
            1 if the byte is inside an executable section
            0 if it's not.
        """

        total_size = os.path.getsize(path)
        mask = np.full((total_size,), 0, dtype='uint8')

        filetype = magic.from_file(str(path))
        if 'ELF' in filetype:
            # ELF
            f = filebytes.elf.ELF(path)
            exec_sections = [
                sec
                for sec in f.sections
                if sec.header.sh_flags & filebytes.elf.SHF.EXECINSTR
            ]
            for section in exec_sections:
                mask[section.header.sh_offset: section.header.sh_offset + section.header.sh_size] = 1

        elif 'Windows' in filetype and 'PE' in filetype:
            # PE
            f = filebytes.pe.PE(path)
            exec_sections = [
                sec
                for sec in f.sections
                if sec.header.Characteristics & filebytes.pe.IMAGE_SCN.CNT_CODE
            ]
            for section in exec_sections:
                mask[section.header.PointerToRawData: section.header.PointerToRawData + section.header.SizeOfRawData] = 1

        elif 'Mach-O' in filetype:
            # Mach-O
            macho_file = filebytes.mach_o.MachO(path)
            load_commands = [
                command
                for command in macho_file.loadCommands
                if command.header.cmd == filebytes.mach_o.LC.SEGMENT_64
            ]
            exec_sections = [
                section.header
                for command in load_commands
                for section in command.sections
                if section.header.flags & filebytes.mach_o.CpuType.S_ATTR_SOME_INSTRUCTIONS
            ]
            for section in exec_sections:
                mask[section.offset: section.offset + section.size] = 1

        else:
            raise NotImplementedError("Unsupported file type: {}".format(path))

        return mask

    def preprocess(self, paths, sample_size=None):
        # Sample if needed
        if sample_size is not None:
            _, paths = train_test_split(paths, test_size=sample_size)

        X = [self._binary_to_matrix(path) for path in paths]

        if self.extract_ground_truth:
            y = [self._code_mask(path) for path in paths]
            return paths, X, y
        else:
            return paths, X