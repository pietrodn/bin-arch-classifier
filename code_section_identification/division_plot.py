import matplotlib.pyplot as plt
import matplotlib
from matplotlib.colors import Normalize
import numpy as np
from sklearn import metrics

def plot_barcode(data, ax, options={}):
    barprops = dict(aspect='auto', cmap=plt.cm.summer, interpolation='none')
    barprops.update(options)

    data_reshaped = np.reshape(data, (1, data.shape[0]))
    ax.imshow(data_reshaped, **barprops)

def plot_binaries(crf, x_test, y_test, path):
    bin_name = path.split('/')[-1]
    print(bin_name)
    
    sz = y_test.size
    code_fraction = y_test.mean()
    
    predicted_bytes_pre = crf.predict([x_test])[0]
    predicted_bytes_post = crf.postprocess_prediction(predicted_bytes_pre)

    accuracy_pre = metrics.accuracy_score(y_test, predicted_bytes_pre)
    accuracy_post = metrics.accuracy_score(y_test, predicted_bytes_post)
    
    actual_pos = (y_test==1).sum()
    actual_neg = (y_test==-1).sum()
    
    fp_pre = ((predicted_bytes_pre - y_test) == 1).sum()
    fn_pre = ((predicted_bytes_pre - y_test) == -1).sum()
    precision_pre = metrics.precision_score(y_test, predicted_bytes_pre)
    recall_pre = metrics.recall_score(y_test, predicted_bytes_pre)
    
    fp_post = ((predicted_bytes_post - y_test) == 1).sum()
    fn_post = ((predicted_bytes_post - y_test) == -1).sum()
    precision_post = metrics.precision_score(y_test, predicted_bytes_post)
    recall_post = metrics.recall_score(y_test, predicted_bytes_post)
    
    errors_pre = predicted_bytes_pre - y_test
    errors_post = predicted_bytes_post - y_test

    fig = plt.figure(figsize=(10, 6))
    ax1 = fig.add_subplot(511)
    ax2 = fig.add_subplot(512)
    ax3 = fig.add_subplot(513)
    ax4 = fig.add_subplot(514)
    ax5 = fig.add_subplot(515)

    plot_barcode(y_test, ax1)
    plot_barcode(predicted_bytes_pre, ax2, options={'norm': Normalize(0, 1)})
    plot_barcode(errors_pre, ax3, options={'norm': Normalize(-1, 1)})
    plot_barcode(predicted_bytes_post, ax4, options={'norm': Normalize(0, 1)})
    plot_barcode(errors_post, ax5, options={'norm': Normalize(-1, 1)})

    ax1.set_title('Ground truth (code fraction={:.3f}%)'.format(
        code_fraction * 100,
    ))
    ax2.set_title('Prediction (accuracy={:.3f}%, precision={:.3f}%, recall={:.3f}%, FP={}, FN={})'.format(
        accuracy_pre * 100,
        precision_pre * 100, recall_pre * 100,
        fp_pre, fn_pre,
    ))
    ax3.set_title("Errors before postprocessing")
    ax4.set_title('Prediction after post-processing (accuracy={:.3f}%, precision={:.3f}%, recall={:.3f}%, FP={}, FN={})'.format(
        accuracy_post * 100,
        precision_post * 100, recall_post * 100,
        fp_post, fn_post,
    ))
    ax5.set_title("Errors after postprocessing")
    
    ax1.get_yaxis().set_visible(False)
    ax2.get_yaxis().set_visible(False)
    ax3.get_yaxis().set_visible(False)
    ax4.get_yaxis().set_visible(False)
    ax5.get_yaxis().set_visible(False)
    
    fig.suptitle('Code section identification: {}'.format(bin_name))
    fig.tight_layout()
    fig.subplots_adjust(top=0.88)
    plt.savefig('figures/{}.pdf'.format(bin_name))

    return (accuracy_pre, accuracy_post)