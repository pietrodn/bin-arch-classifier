# Supervised learning approaches to assist the static analysis of executable files

This repository is the code of my master thesis: *Supervised learning approaches to assist the static analysis of executable files](https://www.politesi.polimi.it/handle/10589/135988)*.

## Installation notes

### Required system libraries

You need to install the Python development headers (`python3-dev` on Debian/Ubuntu) to compile some dependencies.
On Mac this is already included.

You will also need `libmagic`. On macOS, do:
```bash
brew install libmagic
```

### Python dependencies

Note about pystruct:

pystruct requires [this patch](https://github.com/pystruct/pystruct/pull/4/files) to be able to work with sparse matrixes.
To build a wheel containing the patch, please do:
```bash
pip wheel -w dependencies git+https://github.com/pystruct/pystruct.git@refs/pull/4/merge#egg=pystruct
```

And then adapt the `Pipfile` to point to the built wheel in the `dependency` folder, for example:
```toml
pystruct = { path="./dependencies/pystruct-0.3.2-cp36-cp36m-macosx_10_15_x86_64.whl", requires=["numpy"] }
```
The path will change depending on your operating system.

Install all packages with Pipenv:

```
pipenv sync --dev
```

## Explore the notebooks

After installing, you can explore the Jupyter notebook by launching the server in local:

```bash
pipenv run jupyter notebook
```

## Contents

### Architecture classification

This part is my attempt to replicate the results of the paper [*Automatic classification of object code using machine learning*](http://www.sciencedirect.com/science/article/pii/S1742287615000523) by John Clemens (2015).

## Datasets

### Clemens's dataset

The dataset was obtained by the author by emailing him as of [his instructions](http://www.deater.net/john/pages/object-code-classification.html).

It only contains the code section of binaries, stripped from other sections.

### NetBSD

In `dataset`, there is a script which downloads several NetBSD packages, extracts them, and divides the ELF binaries by architectures.

To execute it:

1. Configure the `$DOWNLOAD_DIR` in `preprocess/debian_download.sh`
2. Optionally, change the names of the packages that you want in `dataset/netbsd_packages.txt`
3. `./preprocess/netbsd_download.sh`

### Debian

In `dataset`, there is a script which downloads several Debian packages, extracts them, and divides the ELF binaries by architectures.

It has to be executed on a system where dpkg and apt-get are present.

To execute it:

1. Configure the `$DOWNLOAD_DIR` in `preprocess/debian_download.sh`
2. Optionally, change the names of the packages that you want in `preprocess/debian_packages.txt`
3. `./preprocess/debian_prepare.sh` (warning: this **will change your system's supported architectures for .deb packages!**)
4. `./preprocess/debian_download.sh`

## Data preprocessing

This is done by the script `dataset_loader.py`.

The program performs data munging on the executable, producing, for each sample:

* Count of occurrences of each byte (256 values)
* Count of the occurrences of endianness markers (4 values)
* Size of the sample (1 value)

All of this is written in a CSV file (`data.csv`).

Usage:

    python3 dataset_loader.py -o data.csv <dataset_folder>

Expected structure of dataset folder:

* `dataset_folder/architecture_name/binary_file`
* e.g.: `dataset/intel_x86/emacs`

## Data analysis

Steps of data analysis notebook (`Dataset analysis.ipynb`):

1. Read data (`data.csv`).
2. Preprocessing: divide all values by sample size.
3. Learning: apply logistic regression to the features.
    * Code to optimize the regularization coefficient
4. Testing: evaluation of performance measures.
    * Precision
    * Recall
    * F1-measure
    * Confusion matrix
