from BinaryClassifier import BinaryClassifier
from collections import defaultdict

# Usage:
# python block_classifier.py classifiers/classif_debian_random.pkl blob.bin -s 2048 -n 12

class BlockClassifier:
    def __init__(self, classifier_path, w_size=1024, w_step=None):

        self.bc = BinaryClassifier.load_classifier(classifier_path)

        self.w_size = w_size
        if w_step is None:
            w_step = w_size # default window step: window size
        self.w_step = w_step


    def get_top_chunks(self, blob, n=10):
        prob_classes = self.get_chunk_predictions(blob)
        prob_classes.sort(key=lambda tup: tup[0], reverse=True)
        return prob_classes[:n]

    def get_chunk_predictions(self, blob):
        prob_classes = []
        for i in range(0, len(blob), self.w_step):
            chunk = blob[i:i + self.w_size]
            feature_vector = self.bc.gen_feature_vector(chunk)
            prob_arr = dict(zip(self.bc.classes_, self.bc.predict_proba(feature_vector)[0]))
            cls = max(prob_arr, key=prob_arr.get)
            prob_classes.append((prob_arr[cls], cls, i))
        return prob_classes

    def predict_proba(self, blob):
        chunks = self.get_top_chunks(blob, n=25)

        probas = defaultdict(int)
        for chunk in chunks:
            probas[chunk[1]] += chunk[0]

        sum_points = sum(probas.values())
        result = {k: v/sum_points for k, v in probas.items()}
        return result

def main():
    import argparse
    parser = argparse.ArgumentParser(description='Classify a binary block by dividing it in chunks and selecting the chunks with top accuracy.')
    parser.add_argument('classifier', help="The classifier to use, in .pkl format")
    parser.add_argument('binary_file', help="Binary to classify")
    parser.add_argument('-n', '--topn', default=10, type=int, help="The top-n blocks to print")
    parser.add_argument('-w', '--window-size', default=1024, type=int, help="Sliding window size")
    parser.add_argument('-s', '--window-step', default=None, type=int, help="Sliding window step")
    args = parser.parse_args()

    blockc = BlockClassifier(args.classifier, args.window_size, args.window_step)
    with open(args.binary_file, 'rb') as f:
        blob = f.read()
    tops = blockc.get_top_chunks(blob, args.topn)
    print("File: {}".format(args.binary_file))
    print("Top {} matches for blocks of size {} B (step {} B):\n".format(args.topn, blockc.w_size, blockc.w_step))
    print("\n".join(["0x{:08x}: {} ({:08f}%)".format(i[2], i[1], i[0] * 100) for i in tops]))

    print("\n\nLikelihood of architectures:\n" + str(blockc.predict_proba(blob)))

if __name__=='__main__':
    main()
