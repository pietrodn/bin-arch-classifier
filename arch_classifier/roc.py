import matplotlib.pyplot as plt
from scipy import interp
import numpy as np
from itertools import cycle
from BinaryClassifier import BinaryClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve
from sklearn.metrics import auc


def compute_roc(bc, X_test, y_test):
    """Return the TPR, FPR, AUC relative to each class of the test set."""
    y_test_bin = label_binarize(y_test, bc.classes_)
    y_score = bc.predict_proba(X_test)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(len(bc.classes_)):
        fpr[i], tpr[i], _ = roc_curve(y_test_bin[:, i], y_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    # Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_test_bin.ravel(), y_score.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    return fpr, tpr, roc_auc


def make_roc_analysis(in_csv, test_size=0.5):
    X, y = BinaryClassifier().parse_training_data(in_csv)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)

    bc = BinaryClassifier()
    bc.fit(X_train, y_train)
    report, cm = bc.evaluate_test(X_test, y_test)
    print(report)
    fpr, tpr, roc_auc = compute_roc(bc, X_test, y_test)
    plot_roc(tpr, fpr, roc_auc, bc.classes_)


def plot_roc(tpr, fpr, roc_auc, classes):
    """Plot ROC curves for each classifier and micro-, macro-averages.
    
    The code was taken and adapted from the examples in the scikit-learn documentation:
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html)."""

    # Compute macro-average ROC curve and ROC area

    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(classes))]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(len(classes)):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])

    # Finally average it and compute AUC
    mean_tpr /= len(classes)

    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

    # Plot all ROC curves
    plt.figure(figsize=(10,10))
    plt.plot(fpr["micro"], tpr["micro"],
             label='micro-average ROC curve (area = {0:0.4f})'
                   ''.format(roc_auc["micro"]),
             color='deeppink', linestyle=':', linewidth=4)

    plt.plot(fpr["macro"], tpr["macro"],
             label='macro-average ROC curve (area = {0:0.4f})'
                   ''.format(roc_auc["macro"]),
             color='navy', linestyle=':', linewidth=4)

    colors = cycle(['aqua', 'darkorange', 'cornflowerblue', 'y', 'darkgray', 'lightpink', 'cadetblue'])
    for i, color in zip(range(len(classes)), colors):
        plt.plot(fpr[i], tpr[i], color=color, lw=2,
                 label='{0} (area = {1:0.4f})'
                 ''.format(classes[i], roc_auc[i]))

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('ROC curve for each architecture, and averages')
    plt.legend(loc="lower right")
    plt.show()