# Dataset for the architecture classifier

The CSV files in this directory contain the data to train the classifier, obtained after running the preprocessing phase on the binaries.

## Format of the CSV files
The first two columns of the files contain the sample name and its length; the other columns contain the features for the classifier (i.e., frequencies of bytes and regex patterns).
