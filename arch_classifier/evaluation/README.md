# Architecture classifier: evaluation results

The `.json` files in this folder contain the evaluation results computed by executing the [`DatasetEvaluation.ipynb`](../DatasetEvaluation.ipynb) notebook on all the datasets.

The `.tex` files are automatically generated from the corresponding `.json` files by the [`gen_latex_tables.py`](../gen_latex_tables.py) script.
