import argparse
from pathlib import Path
import json
import numpy as np

metric_names = {
    'accuracy': 'Accuracy',
    'precision_macro': 'Precision (macro)',
    'recall_macro': 'Recall (macro)',
    'f1_macro': 'F1 (macro)',
    'precision': 'Precision',
    'recall': 'Recall',
    'f1': 'F1',
    'auc': 'AUC',
    'support': 'Support',
}

def main():
    parser = argparse.ArgumentParser(
        description='Generate LaTeX tables from the results of the evaluation phase.')
    parser.add_argument('evaluation_folder')
    args = parser.parse_args()

    for json_file in Path(args.evaluation_folder).glob('*.json'):

        json_res = json.loads(
            json_file.read_text(),
            parse_float=lambda x: round(float(x), 4) # precision of floats
        )

        tex_source = "% {}\n\n".format(json_file.name) + parse_results(json_res)
        outfile = json_file.with_suffix('.tex')
        outfile.write_text(tex_source)

        print("{} -> {}".format(json_file, outfile))


def parse_results(res):
    dataset = gen_dataset_description(res['dataset'])
    #global_metrics = gen_global_metrics(res['global_metrics'])
    class_metrics = gen_class_metrics(res['class_metrics'], res['classes'])
    confusion_matrix = gen_confusion_matrix(res['class_metrics']['confusion_matrix'], res['classes'])
    res = "\n\n".join([dataset, class_metrics, confusion_matrix])
    return res


def gen_dataset_description(dataset):
    table = sorted([(arch, int(num)) for (arch, num) in dataset.items()])
    headers = ('\\textbf{Architecture}', '\\textbf{Samples}')
    footer = ('\\textbf{Total}', sum(x[1] for x in table))
    return gen_latex_table(table, headers=headers, footer=footer)

def gen_global_metrics(cv_metrics):
    table = [(metric_names[metric], x) for (metric, x) in cv_metrics.items()]
    headers = ('\\textbf{Metric}', '\\textbf{Value}')
    return gen_latex_table(table, headers=headers)

def gen_class_metrics(class_metrics, classes):
    #metrics = [k for k in class_metrics.keys() if k != 'confusion_matrix']
    metrics = ['support', 'precision', 'recall', 'auc', 'f1']

    table = [(c,) + tuple(class_metrics[m][i] for m in metrics) for (i, c) in enumerate(classes)]
    headers = ('\\textbf{Class}',) + tuple('\\textbf{' + metric_names[m] + '}' for m in metrics)
    footer = ('\\textbf{Total}',)\
            + (np.sum(class_metrics['support']),) \
            + tuple(np.mean(class_metrics[m]).round(4) for m in metrics if m != 'support')

    return gen_latex_table(table, headers=headers, footer=footer)


def gen_confusion_matrix(cm, classes):
    headers = ('',) + tuple(c for c in classes)
    table = [(c,) + tuple(cm[i]) for (i, c) in enumerate(classes)]
    format_string = 'l|' + 'l' * len(classes)
    return gen_latex_table(table, headers=headers, format_string=format_string)


def gen_latex_table(values, headers, format_string=None, footer=None):

    def format_row(t):
        vals = " & ".join((str(x) for x in t))
        return vals + " \\\\"

    if format_string is None:
        format_string = ''.join('l' * len(headers))

    out = "\\begin{tabular}{" + format_string + "}"
    out += "\n" + format_row(headers) + "\n\hline\n"
    out += "\n".join([format_row(row) for row in values])
    if footer:
        out += "\n\hline\n" + format_row(footer)
    out += "\n\\hline\n\\end{tabular}"
    return out

if __name__ == '__main__':
    main()
