from pathlib import Path
import argparse
import multiprocessing
from BinaryClassifier import BinaryClassifier
import pandas as pd
from progress.bar import IncrementalBar

class DatasetLoader:
    def __init__(self, num_workers, size_threshold=0, max_samples_per_arch=-1, verbose=True):
        self.num_workers = num_workers
        self.bc = BinaryClassifier()
        self.size_threshold = size_threshold # bytes
        self.max_samples_per_arch = max_samples_per_arch
        self.table = pd.DataFrame(
            columns=['length'] + self.bc.get_feature_names() + ['arch']
        )
        self.verbose = verbose

    def process_file(self, file_path, arch_name):
        binary_name = arch_name + '/' + file_path.name
        feature_vector = self.bc.gen_feature_vector(file_path.read_bytes())

        return {
            'name': binary_name,
            'arch': arch_name,
            'features': feature_vector,
            'length': file_path.stat().st_size,
        }

    # Callback from workers
    def add_to_table(self, data):
        if data is None:
            raise Exception("No data returned!!")
            return

        row = [data['length']] + data['features'].ravel().tolist() + [data['arch']]
        self.table.loc[data['name']] = row

        self.bar.next()

    def load_from_dir(self, binary_dir):
        """Loads binaries from a directory into a Pandas DataFrame."""
        binary_dir = Path(binary_dir)
        arch_codes = [arch_dir.name for arch_dir in binary_dir.iterdir() if arch_dir.is_dir()]

        binaries = []
        for arch in arch_codes:
            count = 0
            for f_path in (binary_dir / arch).iterdir():
                # pick only n samples per architecture
                if count >= self.max_samples_per_arch and not self.max_samples_per_arch == -1:
                    break

                if not f_path.is_file() or f_path.stat().st_size < self.size_threshold:
                    continue

                binaries.append((f_path, arch))
                count += 1

        self.bar = IncrementalBar(
            'Processing: {}'.format(binary_dir.name),
            max=len(binaries),
            suffix='%(percent)d%%  Elapsed: %(elapsed_td)s  ETA: %(eta_td)s',
        )
        with multiprocessing.Pool(processes=self.num_workers) as pool:
            for (f_path, arch) in binaries:
                pool.apply_async(self.process_file, [f_path, arch], callback=self.add_to_table)

            # Needed: the default closing function for the context manager is to kill all the processes
            pool.close()
            pool.join()

            self.bar.finish()

    def to_csv(self, csvfile):
        self.table.to_csv(csvfile)


def main():
    parser = argparse.ArgumentParser(description='Load a dataset from a folder of binaries and write a CSV file as result.')
    parser.add_argument('dataset_directory')
    parser.add_argument('-o', '--out', default='data.csv')
    parser.add_argument('-j', '--num-workers', default=multiprocessing.cpu_count(), type=int)
    parser.add_argument('-s', '--size-threshold', default=0, type=int, help='Size threshold for the binaries')
    args = parser.parse_args()

    loader = DatasetLoader(args.num_workers, size_threshold=args.size_threshold)
    print('Loading data from {}'.format(args.dataset_directory))
    loader.load_from_dir(args.dataset_directory)

    print('Writing results into {}'.format(args.out))
    loader.to_csv(args.out)

if __name__=='__main__':
    main()
