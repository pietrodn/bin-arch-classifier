# Architecture classifier: performance vs. sample size

This folder contains the dataset and the results relative to the evaluation of the performance of the architecture classifier vs. the sample size of binaries.

* `dataset_debian_random_text_<N>_bytes.csv`: training data extracted from the DebianRandom dataset as described in the thesis;
* `debian_random_text_results.csv`: macro-averaged F1 of the classifier vs. sample size;
* `size_plot_debian_random_text.pdf`: graph illustrating the results in `debian_random_text_results.csv`.
