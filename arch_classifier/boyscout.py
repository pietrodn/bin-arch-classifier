import argparse
import multiprocessing
import angr
from scandir import scandir
import csv

# Make angr less verbose
import logging
logging.basicConfig(level=logging.ERROR)
logging.getLogger('angr').setLevel(logging.ERROR)
logging.getLogger('cle.loader').setLevel(logging.ERROR)

def process_file(file_path, arch_name, file_name):
    name = arch_name + '/' + file_name
    d = {
        'name': name,
        'real_arch': arch_name
    }

    # Try to determine architecture from binary headers
    try:
        proj = angr.Project(file_path)
        d.update({
            'angr_arch': proj.arch.name.lower(),
            'angr_endianness': proj.arch.memory_endness
        })
    except Exception as e:
        print("Problem with {}: {}".format(name, e))

    # Try to determine architecture with Boyscout
    try:
        # Re-load as blob to include the entire binary file in the analysis, not only the code section
        proj = angr.Project(
            file_path,
            load_options={
                'main_opts': {
                    'backend': 'blob',
                    'custom_arch': 'x86',
                }
            })
        boyscout = proj.analyses.BoyScout()
        d.update({
            'bs_arch': boyscout.arch.lower(),
            'bs_endianness': boyscout.endianness
        })
    except Exception as e:
        print("Problem with {}: {}".format(name, e))

    return d

class BoyScoutEvaluation:
    def __init__(self, num_workers, size_threshold=1, max_samples_per_arch=-1):
        self.num_workers = num_workers
        self.size_threshold = size_threshold # bytes
        self.max_samples_per_arch = max_samples_per_arch
        self.data = []

    # Callback from workers
    def add_to_table(self, d):
        if d:
            self.data.append(d)
            print(d)

    def load_from_dir(self, binary_dir):
        """Loads binaries from a directory into a Pandas DataFrame, and returns it."""
        arch_codes = [arch_dir.name for arch_dir in scandir(binary_dir) if arch_dir.is_dir()]

        pool = multiprocessing.Pool(processes=self.num_workers)
        for arch in arch_codes:
            print("Scanning files in {}...".format(arch))
            count = 0
            for f_path in scandir(binary_dir + '/' + arch):
                # pick only n samples per architecture
                if count >= self.max_samples_per_arch and not self.max_samples_per_arch == -1:
                    break

                if not f_path.is_file():
                    continue

                pool.apply_async(process_file, [f_path.path, arch, f_path.name], callback=self.add_to_table)
                count += 1

        # Needed: the default closing function for the context manager is to kill all the processes
        pool.close()
        pool.join()

        return self.data

def main():
    parser = argparse.ArgumentParser(description='Load a dataset from a folder of binaries and write a CSV file as result.')
    parser.add_argument('dataset_directory')
    parser.add_argument('-o', '--out', default='data.csv')
    parser.add_argument('-j', '--num-workers', default=None, type=int)
    parser.add_argument('-n', '--max-samples', default=-1, type=int, help='max samples per architecture')
    args = parser.parse_args()

    loader = BoyScoutEvaluation(num_workers=args.num_workers, max_samples_per_arch=args.max_samples)
    print('Loading data from {}'.format(args.dataset_directory))
    data = loader.load_from_dir(args.dataset_directory)

    print('Writing results into {}'.format(args.out))
    with open(args.out, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=['name', 'real_arch', 'angr_arch', 'angr_endianness', 'bs_arch', 'bs_endianness'])
        writer.writeheader()
        writer.writerows(data)

if __name__=='__main__':
    main()
