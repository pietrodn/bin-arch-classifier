# Evaluation of angr's boyscout.py

This directory contains the data used to evaluate the accuracy of angr's [`boyscout.py`](https://github.com/angr/angr/blob/master/angr/analyses/boyscout.py).

See also the iPython notebook [`BoyScout_Analysis.ipynb`](./BoyScout_Analysis.ipynb) and the Python script [`boyscout.py`](./boyscout.py).

## [boyscout.csv](./boyscout.csv)

This file contains the results of the evaluation of Boyscout over the Debian dataset.

Columns:

* `name`: the relative path of the binary;
* `real_arch`: true architecture of the binary obtained from its metadata;
* `angr_arch`, `angr_arch`: the architecture and the endianness identified by Angr when loading the binary file normally;
* `bs_arch`, `bs_endianness`: the architecture and the endianness identified by Boyscout.py after loading the binary file as binary blob and running `boyscout.py`.
