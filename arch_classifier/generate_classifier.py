import argparse
from BinaryClassifier import BinaryClassifier

# Usage example:
# python generate_classifier.py -o debian.pkl dataset/data_debian.csv

def main():
    parser = argparse.ArgumentParser(description='Load a dataset from a .csv file and return a Pickle file containing the BinaryClassifier.')
    parser.add_argument('input_csv')
    parser.add_argument('-o', '--out', default='classifier.pkl')
    args = parser.parse_args()

    print('Loading data from {}'.format(args.input_csv))
    bc = BinaryClassifier()
    X, y = bc.parse_training_data(args.input_csv)

    print('Training the classifier...')
    bc.fit(X, y)

    print('Writing results into {}'.format(args.out))
    bc.save_classifier(args.out)

if __name__=='__main__':
    main()
