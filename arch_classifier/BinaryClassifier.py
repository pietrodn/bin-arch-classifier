#!/usr/bin/env python3

import pandas as pd
import numpy as np
import re
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib
from sklearn.base import BaseEstimator
from sklearn.base import ClassifierMixin
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.exceptions import NotFittedError

class BinaryClassifier(BaseEstimator, ClassifierMixin):

    def __init__(self, C=10000, penalty='l1', n_jobs=1, use_regexes=True, min_size=1, max_size=None):
        self.C = C
        self.penalty = penalty
        self.n_jobs = n_jobs
        self.use_regexes = use_regexes
        self.min_size = min_size
        self.max_size = max_size

        # Regexes to count as features.
        # Many of these are taken from angr's archinfo: https://github.com/angr/archinfo
        self.regexes = [
                # Endianness
                b"\xff\xfe",
                b"\xfe\xff",
                b"\x00\x01",
                b"\x01\x00",

                # aarch64
                b"\xC0\x03\x5F\xD6",
                b"\x1F\x20\x03\xD5",

                # amd64
                b"\x55\x48\x89\xe5",
                b"\x48[\x83,\x81]\xec[\x00-\xff]",
                b"\xc9\xc3",
                b"([^\x41][\x50-\x5f]{1}|\x41[\x50-\x5f])\xc3",
                b"\x48[\x83,\x81]\xc4([\x00-\xff]{1}|[\x00-\xff]{4})\xc3",

                # arm, BE
                b"\xe9\x2d[\x00-\xff][\x00-\xff]",
                b"\xe5\x2d\xe0\x04",
                b"\xe8\xbd[\x00-\xff]{2}\xe1\x2f\xff\x1e",
                b"\xe4\x9d\xe0\x04\xe1\x2f\xff\x1e",

                # arm, LE
                b"\x1E\xFF\x2F\xE1",
                b"[\x00-\xff][\x00-\xff]\x2d\xe9",
                b"\x04\xe0\x2d\xe5",
                b"[\x00-\xff]{2}\xbd\xe8\x1e\xff\x2f\xe1",
                b"\x04\xe0\x9d\xe4\x1e\xff\x2f\xe1",

                # mips32, BE
                b"\x27\xbd\xff[\x00-\xff]",
                b"\x3c\x1c[\x00-\xff][\x00-\xff]\x9c\x27[\x00-\xff][\x00-\xff]",
                b"\x8f\xbf[\x00-\xff]{2}([\x00-\xff]{4}){0,4}\x03\xe0\x00\x08",

                # mips32, LE
                b"[\x00-\xff]\xff\xbd\x27",
                b"[\x00-\xff][\x00-\xff]\x1c\x3c[\x00-\xff][\x00-\xff]\x9c\x27",
                b"[\x00-\xff]{2}\xbf\x8f([\x00-\xff]{4}){0,4}\x08\x00\xe0\x03",

                # mips32 + mips64
                b"\x08\x00\xE0\x03\x25\x08\x20\x00",

                # ppc32 BE
                b"\x94\x21[\x00-\xff]{2}\x7c\x08\x02\xa6",
                b"[\x00-\xff]{2}\x03\xa6([\x00-\xff]{4}){0,6}\x4e\x80\x00\x20",

                # ppc32 LE
                b"[\x00-\xff]{2}\x21\x94\xa6\x02\x08\x7c",
                b"\xa6\x03[\x00-\xff]{2}([\x00-\xff]{4}){0,6}\x20\x00\x80\x4e",
                b"\x20\x00\x80\x4e",
                b"\x00\x00\x00\x60",

                # ppc64 BE
                b"\x94\x21[\x00-\xff]{2}\x7c\x08\x02\xa6",
                b"(?!\x94\x21[\x00-\xff]{2})\x7c\x08\x02\xa6",
                b"\xf8\x61[\x00-\xff]{2}",

                # pcc64 LE
                b"\x20\x00\x80\x4e",
                b"\x00\x00\x00\x60",
                b"[\x00-\xff]{2}\x21\x94\xa6\x02\x08\x7c",
                b"\xa6\x03[\x00-\xff]{2}([\x00-\xff]{4}){0,6}\x20\x00\x80\x4e",

                # x86
                b"\x55(\x8b\xec|\x89\xe5|\x57\x56)",
                b"\xb8[\x00-\xff]\x00\x00\x00[\x50\x51\x52\x53\x55\x56\x57]{0,7}\x8b[\x00-\xff]{2}",
                b"[\x50\x51\x52\x53\x55\x56\x57]{1,7}\x83\xec[\x00-\xff]{2,4}",
                b"[\x50\x51\x52\x53\x55\x56\x57]{1,7}\x8b[\x00-\xff]{2}",
                b"(\x81|\x83)\xec",
                b"\xc9\xc3",
                b"([^\x41][\x50-\x5f]{1}|\x41[\x50-\x5f])\xc3",
                b"[^\x48][\x83,\x81]\xc4([\x00-\xff]{1}|[\x00-\xff]{4})\xc3",
            ]
        self._compiled_regexes = [re.compile(r) for r in self.regexes]

    def get_feature_names(self):
        return ['byte_{}'.format(i) for i in range(256)] + ['regex_{}'.format(i) for i in range(len(self.regexes))]

    def gen_feature_vector(self, blob):
        """Generate the un-normalized feature vector for the given blob."""

        byte_counts = self._byte_count(blob)
        regex_counts = self._regex_counts(blob)
        feature_vector = np.hstack([byte_counts, regex_counts])

        # Normalize all features to length
        length = len(blob)
        x = (feature_vector / length).reshape(1, -1)
        return x

    def _byte_count(self, blob):
        """Byte frequency histogram for binary blob."""

        byte_arr = np.frombuffer(blob, dtype='uint8')
        return np.bincount(byte_arr, minlength=256)

    def _regex_counts(self, blob):
        """For each regex in self.regexes, return the count of matches in the current binary blob."""

        return np.array([len(reg.findall(blob)) for reg in self._compiled_regexes])

    def fit(self, X, y, **fit_params):
        """Fit the classifier with the training data"""
        self.clf_ = LogisticRegression(
            C=self.C,
            n_jobs=self.n_jobs,
            penalty=self.penalty,
            multi_class='ovr'
        )

        self.clf_.fit(X, y)
        self.classes_ = self.clf_.classes_
        return self

    def predict(self, X):
        """Predict the architecture of the given blob."""
        if not hasattr(self, "clf_"):
            raise NotFittedError("Call fit before prediction")
        return self.clf_.predict(X)

    def predict_proba(self, X):
        """Predict the probability of each class, for the given blob."""
        if not hasattr(self, "clf_"):
            raise NotFittedError("Call fit before prediction")
        return self.clf_.predict_proba(X)

    def evaluate_test(self, X, y):
        """Evaluates the classifier on the test data.
        Returns the classification report and the confusion matrix."""
        y_pred = self.clf_.predict(X)
        report = classification_report(y, y_pred, digits=3, labels=self.clf_.classes_)
        cm = confusion_matrix(y, y_pred, labels=self.clf_.classes_)
        return report, cm

    def parse_training_data(self, in_file):
        """Load the training data from a CSV file.

        Assumes that the label stays in the last column."""
        df = pd.read_csv(in_file, index_col=0)

        # Filter by max/min size
        if self.min_size:
            df = df[df['length'] >= self.min_size]
        if self.max_size:
            df = df[df['length'] <= self.max_size]

        X = df.iloc[:, 1:-1] # exclude length column, and label
        y = df.iloc[:, -1]

        if not self.use_regexes:
            X = X.iloc[:,0:(256+4)] # endianness features, but not regexes

        return X, y

    def save_classifier(self, out_file):
        """Save the classifier to a file."""
        joblib.dump(self, out_file, compress=True)

    @classmethod
    def load_classifier(cls, in_file):
        """Load the classifier from a file."""
        return joblib.load(in_file)
