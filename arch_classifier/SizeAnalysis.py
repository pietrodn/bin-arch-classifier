import numpy as np
from BinaryClassifier import BinaryClassifier
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
import pathlib
import random
import sklearn
from dataset_loader import DatasetLoader
import pandas as pd

# Parameters for the analysis

sizes = [2**x for x in range(3, 17)]

datasets = {
    'debian_random': '/Volumes/Tesi/Dataset/DebianRandom/bin',
    'debian_random_text': '/Volumes/Tesi/Dataset/DebianRandom_text/bin',
    'clemens': '/Volumes/Tesi/Dataset/arch-dataset-v1.0'
}

dataset_name = 'debian_random_text'
cv = 10
scoring = 'f1_macro'
bc_classifier = BinaryClassifier(
    C=10000,
    use_regexes=True,
)

dataset = datasets[dataset_name]
results_file = 'size_analysis/{}_results.csv'.format(dataset_name)

class SampledDatasetLoader(DatasetLoader):

    def __init__(self, num_workers, size_threshold=0, max_samples_per_arch=-1, sample_size=None):
        super().__init__(num_workers, size_threshold, max_samples_per_arch, verbose=False)
        self.sample_size = sample_size
        
    def process_file(self, file_path, arch_name):
        
        binary_name = arch_name + '/' + file_path.name
        blob = file_path.read_bytes()
        blob = self.sample_bytes(blob, self.sample_size)

        feature_vector = self.bc.gen_feature_vector(blob)
        if self.verbose:
            print(file_path)

        return {
            'name': binary_name,
            'arch': arch_name,
            'features': feature_vector,
            'length': file_path.stat().st_size,
        }

    @staticmethod
    def sample_bytes(blob, size):
        """Return a random subsequence in blob, of length size."""
        if size >= len(blob):
            return blob
        start = random.randrange(len(blob) - size)
        return blob[start:start+size]


def sizeof_fmt(num, suffix='B'):
    """Pretty prints a file size."""
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.0f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

if not pathlib.Path(results_file).exists():
    cv_scores = []

    for s in sizes:
        sampled_dataset = pathlib.Path('size_analysis/dataset_{}_{}_bytes.csv'.format(dataset_name, str(s)))

        if not sampled_dataset.exists():
            # Generate the file if it does not exist
            print("Generating dataset for size {}".format(sizeof_fmt(s)))
            loader = SampledDatasetLoader(num_workers=4, sample_size=s)
            loader.load_from_dir(dataset)
            loader.to_csv(sampled_dataset)
            print("Saved {}".format(sampled_dataset))

        bc = sklearn.base.clone(bc_classifier)
        X, y = bc.parse_training_data(sampled_dataset)
        scores = cross_val_score(bc, X, y, cv=cv, n_jobs=-1, scoring=scoring)
        cv_scores.append(np.mean(scores))

        print("size <= {}: {} samples, performance={:.2f}".format(sizeof_fmt(s), y.shape[0], np.mean(scores)))

    df = pd.DataFrame({'size': sizes, 'score': cv_scores})
    df.to_csv(results_file)

# Plot: sample size vs. accuracy

df = pd.read_csv(results_file)
fig, ax = plt.subplots(figsize=(8,5))
ax.semilogx(df['size'], df['score'], 'bo-', lw=2)
ax.set_xscale('log', basex=2)
ax.set_xticks(sizes)
ax.xaxis.set_major_formatter(plt.FuncFormatter(lambda x, pos: sizeof_fmt(x)))
ax.grid(True)
ax.set_xlabel('Fragment size')
ax.set_ylabel('Macro-averaged F1-Measure')
ax.set_title('Fragment size vs. classifier performance')
fig.tight_layout()
fig.savefig('size_analysis/size_plot_{}.pdf'.format(dataset_name))