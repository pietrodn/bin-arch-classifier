from .CRFModel import CRFModel
import numpy.ma as ma


class CRFPostprocessModel(CRFModel):

    def __init__(self,
                 C=1,
                 max_iter=150,
                 post_process=False,
                 postprocessing_cutoff=0.1,
                 postprocessing_min_sections=4,
                 lookahead=0,
                 lookbehind=0,
                 verbose=False):

        super().__init__(C, max_iter, lookahead, lookbehind, verbose)
        self.post_process = post_process # whether to post-process or not
        self.postprocessing_cutoff = postprocessing_cutoff # if a segment is smaller than postprocessing_cutoff * length_of_biggest_segment, it is dropped
        self.postprocessing_min_sections = postprocessing_min_sections # if the number of sections falls below this, don't cut more

    def predict(self, X):
        """Return a list of code masks with the predicted code/data division."""

        raw_pred = super().predict(X)

        if self.post_process:
            return [self.postprocess_prediction(pred) for pred in raw_pred]
        else:
            return raw_pred

    def postprocess_prediction(self, states):
        """Iteratively delete (invert) the smallest consecutive sub-sequence of states
        until the smallest subsequence is bigger than a fraction of the biggest subsequence."""

        st = states.copy()
        while True:
            # Compute clumps of consecutive elements
            mx = ma.masked_array(st, mask=st)

            # clumps = [ (slice, value, size) ]
            clumps = []
            clumps += [(cl, 1, cl.stop-cl.start) for cl in ma.clump_masked(mx)]
            clumps += [(cl, 0, cl.stop-cl.start) for cl in ma.clump_unmasked(mx)]

            max_clump_size = max(cl[2] for cl in clumps)
            min_clump = min(clumps, key=lambda x: x[2])

            if len(clumps) > self.postprocessing_min_sections and min_clump[2] < max_clump_size * self.postprocessing_cutoff:
                # Invert the clump
                clump_slice = min_clump[0]
                st[clump_slice] = int(not min_clump[1])
            else:
                break
        return st