from filebytes.elf import ELF
import os
import numpy as np
import numpy.ma as ma
from pystruct.models import ChainCRF
from pystruct.learners import FrankWolfeSSVM
from sklearn.base import BaseEstimator
from sklearn.base import ClassifierMixin
from scipy import sparse
from sklearn.externals import joblib
from sklearn.exceptions import NotFittedError


class CRFModel(BaseEstimator, ClassifierMixin):
    """Conditional Random Field model to separate code from data.
    Uses pystruct's ChainCRF model for fitting and prediction.
    """

    def __init__(self,
                 C=1,
                 max_iter=150,
                 lookahead=0,
                 lookbehind=0,
                 verbose=False):

        self.C = C
        self.max_iter = max_iter
        self.verbose=verbose

        # How many bytes of lookahead/lookbehind to use as features
        self.lookahead = lookahead
        self.lookbehind = lookbehind


    def _preprocess(self, feat_m):
        """Generate lookaheads and lookbehinds as required for the specified feature matrix."""

        # List of feature matrices. It contains the feature for the current byte and the lookahead/lookbehind ones.
        features = []

        # picks the correct rows from the identity matrix
        feat_byte_0 = feat_m
        features.append(feat_byte_0)

        # Compute lookahead/lookbehind by shifting the matrix
        for i in range(-self.lookbehind, self.lookahead + 1):
            if i==0:
                continue
            zerofill = sparse.csr_matrix(np.zeros((abs(i), feat_byte_0.shape[1]), dtype='uint8'))
            if i < 0:
                feature = sparse.vstack([zerofill, feat_byte_0.copy()[:i]])
            elif i > 0:
                feature = sparse.vstack([feat_byte_0.copy()[i:], zerofill])
            features.append(feature)

        return sparse.hstack(features)


    def fit(self, X, y, **fit_params):
        """Learns the CRF model from the training data."""

        if len(X) != len(y):
            raise ValueError("X and y must be of same size")

        # Generate lookaheads and lookbehinds
        X = [self._preprocess(x) for x in X]

        y = [v.copy() for v in y]  # joblib breaks unless this is done

        self._ssvm = FrankWolfeSSVM(
            model=ChainCRF(),
            C=self.C,
            max_iter=self.max_iter,
            verbose=self.verbose
        )
        self._ssvm.fit(X, y)

        return self

    def predict(self, X):
        """Return a list of code masks with the predicted code/data division."""

        if not hasattr(self, "_ssvm"):
            raise NotFittedError("Call fit before prediction")

        # Generate lookaheads and lookbehinds
        X = [self._preprocess(x) for x in X]

        return self._ssvm.predict(X)


    def score(self, X, y):
        """Average accuracy of predictions."""

        if not hasattr(self, "_ssvm"):
            raise NotFittedError("Call fit before prediction")

        if len(X) != len(y):
            raise ValueError("X and y must be of same size")

        predicted = self.predict(X)
        scores = np.array(
            [(predicted[i]==y[i]).sum()/len(y[i])
                for i in range(len(y))]
            )
        return scores.mean()


    def predict_splits(self, x):
        """Return the positions of the predicted code/data transitions."""

        mask = self.predict([x])
        splits = np.argwhere(np.diff(mask))
        return splits

    def actual_splits(self, x):
        """Return the positions of the actual code/data transitions."""

        mask = self.code_mask([x])
        splits = np.argwhere(np.diff(mask))
        return splits

    def save(self, out_file):
        """Save the classifier to a file."""
        joblib.dump(self, out_file, compress=True)

    @classmethod
    def load(cls, in_file):
        """Load the classifier from a file."""
        return joblib.load(in_file)