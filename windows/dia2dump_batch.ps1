Param(
  [Parameter(Mandatory=$True,Position=1)]
   [string]$sourceDirectory
)

$DIA2DUMP = "E:\tools\Dia2Dump_64.exe"

$files = Get-ChildItem -Path $sourceDirectory -Include *.pdb -r -File | % { $_.FullName }

foreach ($file in $files) {
	echo "DUMPING: $file"
	& $DIA2DUMP -s $file > "$file.txt"
}