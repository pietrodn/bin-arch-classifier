Param(
  [Parameter(Mandatory=$True,Position=1)]
   [string]$sourceDirectory
)

$MSBUILD = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe"

$files = Get-ChildItem -Path $sourceDirectory -Include *.sln -r -File | Where-Object {$_.FullName -like "*\cpp\*"} | % { $_.FullName }

 foreach ($file in $files) {
	echo "`n`n`nBUILDING: $file"
	& $MSBUILD /v:quiet /clp:Summary /m:2 /p:platform=x86 /p:configuration=debug /p:DebugType=full /p:AppxPackageSigningEnabled=false $file
	
	if ($LastExitCode -ne 0) {
		echo "Build FAILED with an error: $LastExitCode"
	} else {
		echo "SUCCESS!"
	}
 }