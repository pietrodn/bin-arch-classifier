#!/bin/bash

# Usage: ./extract_exe_pdb.sh source dest
# Find all *.pdb files associated with a .exe or a .dll and copy it in the destination directory.

SRC="$1"
DEST="$2"

PDBFILES=$(find "$SRC" -name "*.pdb")

for i in $PDBFILES; do
    NAME=$(basename "$i" | cut -d. -f1)
    DIR=$(dirname "$i")

    if [[ -f "$DIR/$NAME.exe" ]]
    then
        cp $DIR/$NAME.{exe,pdb} $DEST
    elif [[ -f "$DIR/$NAME.dll" ]]
    then
        cp $DIR/$NAME.{dll,pdb} $DEST
    fi
done