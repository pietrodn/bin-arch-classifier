from sklearn import preprocessing
import subprocess
import tempfile
import numpy as np
from scipy import sparse


class ArmPreprocess:

    def __init__(self, arch="elf32-littlearm"):
        self.arch = arch

        # One-hot encoding of opcodes
        self.opcode_binarizer = preprocessing.LabelBinarizer(sparse_output=True)

        # one-hot encoding of the first byte of every instruction
        self.bytes_binarizer = preprocessing.LabelBinarizer(sparse_output=True).fit(list(range(0, 256)))

        self.fitted = False

    def _strip_file(self, path):
        stripped_file = tempfile.NamedTemporaryFile()
        subprocess.run(["gstrip", "-I", self.arch, "-s", "-o", stripped_file.name, path],
                       stdout=subprocess.PIPE,
                       universal_newlines=True, encoding='utf8')
        return stripped_file

    def _disasm_file(self, path, strip=False):

        if strip:
            stripped_file = self._strip_file(path)
            path = stripped_file.name

        objdump = subprocess.run(["gobjdump", "-b", self.arch, "-d", path], stdout=subprocess.PIPE,
                                 universal_newlines=True, encoding='utf8')
        if strip:
            stripped_file.close()

        insts = [tuple(x.strip().split("\t")) for x in objdump.stdout.split("\n") if "\t" in x and ":" in x]

        addresses = [
            {
                'addr': int(x[0][:-1], 16),
                'bytes': bytes.fromhex(x[1]),
                'opcode': x[2],
                # 'operands': x[3],
            }
            for x in insts
        ]
        return sorted(addresses, key=lambda x: x['addr'])

    def code_mask(self, path):
        """Return a mask ranging over the instructions in the binary:
            1 if the byte is an actual instruction
            0 if it's code
        """

        instructions = self._disasm_file(path, strip=False)
        mask = [(0 if '.word' in x['opcode'] else 1) for x in instructions]
        return np.array(mask)

    def gen_features(self, paths):
        """Return a list of matrices, one for each item in paths.

        Each row of each matrix corresponds to an instruction, as decoded from _disasm_file.
        For each instruction, the following features are extracted:
        * opcode
        * first byte

        Both features are encoded as one-hot (binarized);
        each matrix has dimension (num_instructions, 256 + n_opcodes).
        The result is returned as a list of sparse matrices."""

        # Set of all distinct opcodes encountered in all executable files. Needed to fit the opcode_binarizer
        all_opcodes = set()

        raw_features = []
        for path in paths:
            cur_disasm = self._disasm_file(path, strip=True)

            # List of opcodes for the instructions of the current file
            opcodes = [instr['opcode'] for instr in cur_disasm]
            all_opcodes |= set(opcodes)

            # List of the first byte for each instruction of the current file
            first_bytes = [instr['bytes'][0] for instr in cur_disasm]

            raw_features.append((first_bytes, opcodes))

        if not self.fitted:
            self.opcode_binarizer.fit(list(all_opcodes))
            self.fitted = True

        onehot_features = [sparse.hstack([self.bytes_binarizer.transform(cur_binary_features[0]),
                                          self.opcode_binarizer.transform(cur_binary_features[1])]
                                         ).tocsr().astype('uint8')
                           for cur_binary_features in raw_features]

        return onehot_features

    def preprocess(self, paths):
        X = self.gen_features(paths)
        y = [self.code_mask(path) for path in paths]

        return X, y