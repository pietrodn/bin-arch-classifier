#!/usr/bin/env bash

# Usage: ./gen_masks.sh <folder>

FILES=$(ls $1/*.pdb.txt)

for i in $FILES; do
    NAME=$(basename $i | cut -d. -f1)
    echo "PARSING: $i"
    python parse_dia.py $i -o $1/$NAME.mask.gz
done