from pprint import pprint

import pandas as pd
import re
import subprocess
from pathlib import Path

import numpy as np


class ObjdumpEvaluate:

    data_opcodes = {'int3', 'bad', 'word', 'data'}

    def __init__(self, arch):
        self.arch = arch

    def _disasm_file(self, path):

        objdump = subprocess.run(["gobjdump", "-w", "-b", "binary", "-m", self.arch, "-D", str(path)], stdout=subprocess.PIPE,
                                 universal_newlines=True, encoding='utf8')

        intruction_iterator = re.finditer(
            '(?m)^\s*(?P<address>[0-9a-f]+):\s*\t\s*(?P<bytes>[0-9a-f ]+)\s*\t\s*(?P<opcode>\S+)',
            objdump.stdout
        )

        addresses = [
            {
                'addr': int(x.group('address'), 16),
                'bytes': bytes.fromhex(x.group('bytes').replace(' ', '')),
                'opcode': x.group('opcode'),
            }
            for x in intruction_iterator
        ]
        disasm_result = sorted(addresses, key=lambda x: x['addr'])

        # Check consistency of addresses and byte lengths
        for i in range(len(disasm_result) - 1):
            if disasm_result[i]['addr'] + len(disasm_result[i]['bytes']) != disasm_result[i+1]['addr']:
                raise RuntimeError("Inconsistency in instruction lengths and addresses, between instructions: {} and {}".format(disasm_result[i], disasm_result[i+1]))

        return disasm_result

    def _is_data(self, opcode):
        return any(y in opcode for y in self.data_opcodes)

    def code_mask(self, path):
        """Return a mask ranging over the instructions in the binary:
            1 if the byte is an actual instruction
            0 if it's data
        """

        instructions = self._disasm_file(path)

        return np.repeat(
            np.fromiter((int(not self._is_data(ins['opcode'])) for ins in instructions), np.uint8),
            np.fromiter((len(ins['bytes']) for ins in instructions), np.uint32)
        )

    def evaluate_accuracy(self, exe_path: Path, ground_truth_path: Path):
        predicted_mask = self.code_mask(exe_path)

        true_mask = pd.read_csv(ground_truth_path, compression='gzip', dtype='uint8').values.flatten() - 1
        true_mask.resize((predicted_mask.size, ))

        acc = (true_mask == predicted_mask).sum() / predicted_mask.size
        print("Accuracy for {}: {:4f}".format(exe_path.stem, acc))
        return acc


def evaluate_folder(arch, folder_path):
    from multiprocessing import Pool

    ev = ObjdumpEvaluate(arch=arch)
    all_paths = list(folder_path.glob("*.bin"))
    mask_paths = [bin_path.with_suffix(".mask.gz") for bin_path in all_paths]

    with Pool(4) as p:
        accuracies = p.starmap(ev.evaluate_accuracy, zip(all_paths, mask_paths))

    return {path.stem: acc for path, acc in zip(all_paths, accuracies)}


def main():
    results = evaluate_folder('i386', Path('/Volumes/Deposito/Tesi-backup/Windows/extracted_x86/'))
    pprint(results)

    avg_accuracy = np.mean(np.array(list(results.values())))
    print("Average accuracy: {:.4f}".format(avg_accuracy))


if __name__ == '__main__':
    main()
