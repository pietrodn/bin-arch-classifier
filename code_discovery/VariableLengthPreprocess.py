from pathlib import Path
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from scipy import sparse


class WindowsPreprocess:
    """Load and preprocess the ground truth and prepare it for the classifier.

    The class works on a folder containing raw executables with name "<name>.bin".
    The binary file must contain only the .text section.

    For each executable, a file named "<name>.mask.gz" must exist.
    The mask file contains a sequence of 1s and 2s, one for each byte.
        1 = data
        2 = code

    """

    def _binary_to_matrix(self, blob):
        """Convert the given object file to a matrix (num_bytes, 256) where each row is a one-hot array for each byte.

        This is the format required by pystruct."""

        byte_arr = np.frombuffer(blob, dtype='uint8') # array of integers [0, 255]

        # picks the correct rows from the identity matrix
        feat_byte_0 = sparse.csr_matrix((np.ones(len(byte_arr)), (np.arange(byte_arr.shape[0]), byte_arr)))

        return feat_byte_0

    def preprocess(self, folder, sample_size=None):
        """
        Get the training data from the specified folder, sampling if required.

        :param folder: the folder to process
        :param sample_size: number of executable to sample; if None, consider all data
        :return: (X, y, paths):
            X: list of arrays of bytes, one for each .bin file
            y: list of masks (arrays), one for each corresponding .mask.gz file
            paths: list of file paths of the executables
        """
        X = []
        y = []
        paths = []

        p = Path(folder)
        all_paths = list(p.glob("*.bin"))

        # Sample if needed
        if sample_size is not None:
            _, path_samples = train_test_split(all_paths, test_size=sample_size)
        else:
            path_samples = all_paths

        for bin_path in path_samples:
            # Find the corresponding mask file
            mask_path = bin_path.with_suffix(".mask.gz")

            sec_bytes = bin_path.read_bytes()
            m = pd.read_csv(mask_path, compression='gzip', dtype='uint8').values.flatten() - 1

            # The mask is extended or cutted to the same size of the binary
            # If it's shorter, the last bytes are considered as data
            m.resize((len(sec_bytes, )))

            X.append(self._binary_to_matrix(sec_bytes))
            y.append(m)
            paths.append(str(bin_path))

        return X, y, paths
