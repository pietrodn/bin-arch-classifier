# Code discovery: evaluation results

The evaluation results are divided in two parts:

* [`fixed`](./fixed): evaluation on the ARM dataset (fixed-length instructions)
* [`general`](./general): evaluation on the x86, x86-64 datasets (variable-length instructions)

The `.json` files contain the evaluation results computed by executing the [`DatasetEvaluation-Fixed.ipynb`](../DatasetEvaluation-Fixed.ipynb) and [`DatasetEvaluation-General.ipynb`](../DatasetEvaluation-General.ipynb) notebooks on the datasets.

The `.tex` files are automatically generated from the corresponding `.json` files by the [`gen_latex_tables.py`](../../code_section_identification/gen_latex_tables.py) script in the `code_section_identification` folder.
