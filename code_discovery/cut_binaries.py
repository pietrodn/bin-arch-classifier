import argparse
import re
from pathlib import Path
from filebytes.pe import PE


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("bin_folder", help="folder containing binaries")
    parser.add_argument("--section", "-s", help="section to extract", default=1, type=int)
    parser.add_argument("--encoding", help="encoding of the input files", default="utf-16")
    args = parser.parse_args()

    cutter = BinaryCutter(section_n=args.section, encoding=args.encoding)

    p = Path(args.bin_folder)
    for bin_path in list(p.glob("*.exe")) + list(p.glob("*.dll")):
        print("CUTTING: {} -> {}".format(bin_path, bin_path.with_suffix(".bin")))
        cutter.process_binary(bin_path)


class BinaryCutter:

    coff_re = re.compile(
        "^CoffGroup\s*:\s*"
        "\[0x(?P<sec>[0-9A-F]+):"
        "0x(?P<offset>[0-9a-f]+)\]\s*"
        "0x(?P<rva>[0-9A-F]+), "
        "len = (?P<len>[0-9A-F]+), "
        "characteristics = (?P<characteristics>[0-9A-F]+), "
        "(?P<name>.*)$")

    def __init__(self, section_n, encoding='utf16'):
        self.section_n = section_n
        self.encoding = encoding

    def process_binary(self, bin_path):
        coff_groups = self.get_coff_groups(bin_path.with_suffix(".pdb.txt"))

        pe_file = PE(bin_path)
        section_bytes = pe_file.sections[self.section_n].bytes

        # Exclude from the beginning of the text$x section
        cg_exclude = coff_groups['.text$x']
        section_bytes = section_bytes[:int(cg_exclude['offset'], 16)]

        out_binary = bin_path.with_suffix(".bin")
        out_binary.write_bytes(section_bytes)

    def get_coff_groups(self, info_path):
        coff_groups = {}

        with open(info_path, encoding=self.encoding) as diafile:
            for line in diafile:
                match = self.coff_re.match(line)
                if match:
                    coff_groups[match.group('name')] = match.groupdict()

        return coff_groups


if __name__ == "__main__":
    main()