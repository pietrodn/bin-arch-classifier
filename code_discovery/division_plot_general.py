import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from sklearn import metrics

def plot_barcode(data, ax, options={}):
    barprops = dict(aspect='auto', cmap=plt.cm.summer, interpolation='none')
    barprops.update(options)

    data_reshaped = data.copy()
    data_reshaped.shape = (1, data.shape[0])
    ax.imshow(data_reshaped, **barprops)


def plot_binaries(crf, x, y, path, sample_size=None):
    bin_name = path.split('/')[-1]
    print(bin_name)
    
    predicted_bytes = crf.predict([x])[0]
    errors = predicted_bytes - y
    
    sz = y.size
    code_fraction = y.mean()
    
    fp = ((predicted_bytes - y) == 1).sum()
    fn = ((predicted_bytes - y) == -1).sum()
    accuracy = metrics.accuracy_score(y, predicted_bytes)
    precision = metrics.precision_score(y, predicted_bytes)
    recall = metrics.recall_score(y, predicted_bytes)
    
    fig = plt.figure(figsize=(14, 8))
    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)
    
    # If sampling is enabled, show only a portion of the binary
    if sample_size is not None and sz > sample_size:
        y, predicted_bytes, errors = y[:sample_size], predicted_bytes[:sample_size], errors[:sample_size]
    
    plot_barcode(y, ax1, options={'norm': Normalize(0, 1)})
    plot_barcode(predicted_bytes, ax2, options={'norm': Normalize(0, 1)})
    plot_barcode(errors, ax3, options={'norm': Normalize(-1, 1)})

    ax1.set_title('Actual (code fraction={:.3f}%)'.format(
        code_fraction * 100,
    ))
    ax2.set_title('Predicted (accuracy={:.3f}%, precision={:.3f}%, recall={:.3f}%, FP={}, FN={})'.format(
        accuracy * 100,
        precision * 100, recall * 100,
        fp, fn,
    ))
    ax3.set_title("Errors")
    fig.suptitle('Code discovery for {}'.format(bin_name))
    
    ax1.get_yaxis().set_visible(False)
    ax2.get_yaxis().set_visible(False)
    ax3.get_yaxis().set_visible(False)
    
    fig.tight_layout()
    fig.subplots_adjust(top=0.88)
    plt.savefig('figures/{}.pdf'.format(bin_name))

    return (accuracy, fp, fn)