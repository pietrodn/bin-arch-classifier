import argparse
from pathlib import Path
from filebytes.elf import ELF
from filebytes.binary import BinaryError


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("bin_folder", help="folder containing binaries")
    args = parser.parse_args()

    cutter = CUDACutter()

    p = Path(args.bin_folder)
    for bin_path in p.iterdir():
        print("\nCUTTING: {} -> {}".format(bin_path, bin_path.with_suffix(".bin")))
        try:
            cutter.process_binary(bin_path)
            print("\tDONE!")
        except (Exception, BinaryError) as e:
            print("\tERROR: {}".format(e))


class CUDACutter:

    def __init__(self, section_name='.nv_fatbin'):
        self.section_name = section_name

    def process_binary(self, bin_path):
        elf_file = ELF(bin_path)

        good_sections = [s for s in elf_file.sections if s.name == self.section_name]
        if len(good_sections) == 0:
            raise Exception("Not found: section named '{}'".format(self.section_name))

        out_binary = bin_path.with_suffix(".bin")
        out_binary.write_bytes(good_sections[0].bytes)


if __name__ == "__main__":
    main()