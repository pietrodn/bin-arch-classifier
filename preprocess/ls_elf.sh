#!/bin/bash
# Usage: ./ls_elf.sh <dir_name>
# Lists all the ELF files in the current firectory

find "$1" -type f -exec file {} \; | grep ELF | cut -d':' -f1
