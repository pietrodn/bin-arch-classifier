#!/bin/bash

# Remove the first <header_size> 512-bytes blocks from ELF files in a dataset.
# Usage: ./delete_hader.sh dataset_folder header_size

files=$(find $1 -type f)
for f in $files; do
    echo $f
    dd if="$f" of="$f.trunc" skip=$2
    mv "$f.trunc" "$f"
done
