#!/bin/bash

# Usage: ./strip.sh dataset_folder [num_jobs]

./ls_elf.sh $1 | xargs -P ${2:-1} gstrip --strip-all
