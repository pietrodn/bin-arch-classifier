#!/usr/bin/env bash
# Usage: ./dump_text binary_dir [num_workers]
# Recursively replace each binary with a file containing only the .text section

# Generate .text files from binaries
./ls_elf.sh "$1" | xargs -P ${2:-1} -I {} rabin2 -O d/S/.text -o {}.text {}

# Remove old binaries
./ls_elf.sh "$1" | grep -v "*\.text" | xargs rm
