#!/bin/bash

# Official Debian ports: https://www.debian.org/ports/
ARCHITECTURES="amd64 arm64 armel armhf i386 mips mipsel powerpc ppc64el ppc64el s390 s390x"

for arch in $ARCHITECTURES; do
    echo "Adding architecture $arch to dpkg"
    sudo dpkg --add-architecture $arch
done

sudo apt-get update
