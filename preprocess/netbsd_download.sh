#!/bin/bash

MIRROR_URL="http://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD"

PACKAGES=$(cat netbsd_packages.txt)

# Format: architecture/version
ARCHITECTURES="alpha/7.1 amd64/7.1 i386/7.1 m68k/7.1 mipsel/6.1.5_2015Q2 powerpc/7.1 sparc/7.1 sparc64/7.1"

# Download directory for .tgz archives and destination for extracted binary files
DOWNLOAD_DIR=/Volumes/Deposito/Dataset/NetBSD

mkdir -p $DOWNLOAD_DIR
mkdir -p $DOWNLOAD_DIR/tgz
mkdir -p $DOWNLOAD_DIR/bin

for ARCH in $ARCHITECTURES; do

    ARCH_NAME=$(echo $ARCH | cut -d/ -f1) # architecture name (e.g. sparc64)

    mkdir -p $DOWNLOAD_DIR/tgz/$ARCH_NAME
    mkdir -p $DOWNLOAD_DIR/bin/$ARCH_NAME
    mkdir -p $DOWNLOAD_DIR/tmp/$ARCH_NAME

    # Download all packages
    for PKG in $PACKAGES; do
        PKG_NAME=$(echo $PKG | cut -d/ -f2)

        echo "Downloading $PKG_NAME ($ARCH_NAME)"

        wget -q --timestamping -e robots=off -r -l 1 --no-parent -nd \
            -A.tgz --accept-regex ".*/$PKG_NAME-[^-]+\\.tgz" \
            -P "$DOWNLOAD_DIR/tgz/$ARCH_NAME" \
            "$MIRROR_URL/$ARCH/All/"
    done;

    # Extract bin folder from the archives
    for i in $(ls $DOWNLOAD_DIR/tgz/$ARCH_NAME/*.tgz); do
        echo "Extracting $i"
        tar xf $i -C $DOWNLOAD_DIR/tmp/$ARCH_NAME
    done

    # Find only binary files and move them to $ARCH_DIR
    ./ls_elf.sh $DOWNLOAD_DIR/tmp/$ARCH_NAME | xargs -I @@ mv @@ $DOWNLOAD_DIR/bin/$ARCH_NAME

    # Remove tmp dir with extracted files
    rm -rf $DOWNLOAD_DIR/tmp/$ARCH_NAME
done;
