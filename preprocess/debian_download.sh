#!/bin/bash

PACKAGES=$(cat debian_packages.txt)

# Official Debian ports: https://www.debian.org/ports/
ARCHITECTURES="amd64 arm64 armel armhf i386 mips mipsel powerpc ppc64el s390 s390x"

# Download directory for .deb archives and destination for extracted binary files
DOWNLOAD_DIR=/vagrant/debs

mkdir -p $DOWNLOAD_DIR
mkdir -p $DOWNLOAD_DIR/deb
mkdir -p $DOWNLOAD_DIR/bin

for ARCH_NAME in $ARCHITECTURES; do
    mkdir -p $DOWNLOAD_DIR/deb/$ARCH_NAME
    mkdir -p $DOWNLOAD_DIR/bin/$ARCH_NAME
    mkdir -p $DOWNLOAD_DIR/tmp/$ARCH_NAME

    for PKG in $PACKAGES; do
        echo "Downloading $PKG ($ARCH_NAME)"
        (cd $DOWNLOAD_DIR/deb/$ARCH_NAME; apt-get download $PKG:$ARCH_NAME)
    done

    # Extract bin folder from the archives
    for i in $(ls $DOWNLOAD_DIR/deb/$ARCH_NAME/*.deb); do
        echo "Extracting $i"
        dpkg -x $i $DOWNLOAD_DIR/tmp/$ARCH_NAME
    done

    # Find only binary files and move them to $ARCH_DIR
    ./ls_elf.sh $DOWNLOAD_DIR/tmp/$ARCH_NAME | xargs -I @@ mv @@ $DOWNLOAD_DIR/bin/$ARCH_NAME

    rm -rf $DOWNLOAD_DIR/tmp/$ARCH_NAME
done
