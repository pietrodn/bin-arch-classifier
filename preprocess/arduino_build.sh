#!/bin/bash

BUILD_DIR=/Volumes/Tesi/Dataset/Arduino
ARDUINO_ROOT=/Volumes/Tesi/Applications/Arduino.app/Contents/Java

mkdir -p $BUILD_DIR/{elf,hex,eep,build}

for ino_file in $(find $BUILD_DIR/examples -name "*.ino"); do
    echo $ino_file
    $ARDUINO_ROOT/arduino-builder \
        -compile \
        -hardware $ARDUINO_ROOT/hardware \
        -tools=$ARDUINO_ROOT/tools-builder \
        -tools=$ARDUINO_ROOT/hardware/tools/avr  \
        -libraries "$ARDUINO_ROOT/libraries" \
        -fqbn arduino:avr:uno \
        -build-path $BUILD_DIR/build \
        -quiet \
        $ino_file
    mv $BUILD_DIR/build/*.elf $BUILD_DIR/elf
    mv $BUILD_DIR/build/*.hex $BUILD_DIR/hex
    mv $BUILD_DIR/build/*.eep $BUILD_DIR/eep
done
