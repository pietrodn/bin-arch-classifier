#!/bin/bash

# Usage: pack_dataset.sh dataset_folder [num_workers]
# The script looks for all ELF executable files, and packs them with upx in-place.
# If the packing does not succeed, the file is deleted.

files=$(./ls_elf.sh $1)
for f in $files; do
    upx $f
    if [ $? -ne 0 ]; then
        echo "Packing failed. Removing $f"
        rm $f
    fi
done
